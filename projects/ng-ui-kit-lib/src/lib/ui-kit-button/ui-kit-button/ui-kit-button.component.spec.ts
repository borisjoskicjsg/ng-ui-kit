import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiKitButtonComponent } from './ui-kit-button.component';

describe('UiKitButtonComponent', () => {
  let component: UiKitButtonComponent;
  let fixture: ComponentFixture<UiKitButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiKitButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiKitButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
