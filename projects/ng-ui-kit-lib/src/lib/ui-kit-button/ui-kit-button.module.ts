import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UiKitButtonComponent } from "./ui-kit-button/ui-kit-button.component";

@NgModule({
  declarations: [UiKitButtonComponent],
  imports: [CommonModule],
  exports: [UiKitButtonComponent]
})
export class UiKitButtonModule {}
