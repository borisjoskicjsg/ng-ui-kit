# NgUiKitApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.5.

# Build production version of library
```
npm run build-lib
```
It will build the library and put it /dist folder.

# Run example app
To see your library live, run the live example.
```
npm start
```