import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { UiKitButtonModule } from "ng-ui-kit-lib";
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, UiKitButtonModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
